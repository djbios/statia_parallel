﻿/*
 * Created by SharpDevelop.
 * User: djbios
 * Date: 08.03.2016
 * Time: 11:43
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace parallel_ex
{
	
	public class diap
	{
		public int begin;
		public int end;
		public diap(int b, int e)
		{
			begin=b;
			end=e;
		}
	}
	class Program
	{
		static int[] data;
		static void MyTransform(int i)
		{
			data[i] = data[i] / 10;

			if (data[i] < 10000) data[i] = 0;
			if (data[i] >= 10000) data[i] = 100;
			if (data[i] > 20000) data[i] = 200;
			if (data[i] > 30000) data[i] = 300;
		}
		
		static void RunWorkInDiap(object d)
		{
			//Console.WriteLine(Thread.CurrentThread.ManagedThreadId.ToString());
			diap di = (diap)d;
			for(int i = di.begin; i<di.end; i++)
				MyTransform(i);
		}
		
		static void Main(string[] args)
		{
			List<double> parForResults = new List<double>();
			List<double> TwoThreadsResults = new List<double>();
			List<double> HundThreadResults = new List<double>();
			List<double> hundThreadPoolResults = new List<double>();
			List<double> StupidResults = new List<double>();
			
			Console.WriteLine("Основной поток запущен");
			int iterations = 10;
			for(int iter = 0; iter<iterations; iter++)
			{
				// Время выполнения цикла
				Stopwatch sw = new Stopwatch();

				data = new int[100000000];
				int N = data.Length;
				for (int i = 0; i < data.Length; i++)
					data[i] = i;
				
				sw.Start();
				// Распараллелить цикл методом For()
				Parallel.For(0, data.Length, MyTransform);
				sw.Stop();
				Console.WriteLine("Parallel for преобразование данных в цикле: {0} секунд",
				                  sw.Elapsed.TotalSeconds);
				parForResults.Add(sw.Elapsed.TotalSeconds);
				sw.Reset();
				for (int i = 0; i < data.Length; i++)
					data[i] = i;
				sw.Start();
				
				diap d1 = new diap(0,N/2);
				Thread thread1 = new Thread(new ParameterizedThreadStart(RunWorkInDiap));
				thread1.Start(d1);
				
				diap d2 = new diap(N/2,N);
				Thread thread2 = new Thread(new ParameterizedThreadStart(RunWorkInDiap));
				thread2.Start(d2);
				
				thread1.Join();
				thread2.Join();
				Console.WriteLine("2 Threads преобразование данных в цикле: {0} секунд",
				                  sw.Elapsed.TotalSeconds);
				TwoThreadsResults.Add(sw.Elapsed.TotalSeconds);
				sw.Reset();
				for (int i = 0; i < data.Length; i++)
					data[i] = i;
				sw.Start();
				
				List<Thread> thList = new List<Thread>();
				int threadsCount = 100;
				for(int i =0;i<threadsCount;i++)
				{
					diap d = new diap(i,i+N/threadsCount);
					Thread th = new Thread(new ParameterizedThreadStart(RunWorkInDiap));
					th.Start(d);
					thList.Add(th);
				}
				foreach(var now in thList)
					now.Join();
				
				Console.WriteLine("100 Threads преобразование данных в цикле: {0} секунд",
				                  sw.Elapsed.TotalSeconds);
				HundThreadResults.Add(sw.Elapsed.TotalSeconds);
				
				sw.Reset();
				for (int i = 0; i < data.Length; i++)
					data[i] = i;
				sw.Start();
				int nWorkerThreads;
				int nCompletionThreads;
				ThreadPool.GetMaxThreads(out nWorkerThreads, out nCompletionThreads);
				Console.WriteLine("Максимальное количество потоков: " + nWorkerThreads
				                  + "\nПотоков ввода-вывода доступно: " + nCompletionThreads);
				
				threadsCount = 100;
				for(int i =0;i<threadsCount;i++)
				{
					diap d = new diap(i,i+N/threadsCount);

					ThreadPool.QueueUserWorkItem(RunWorkInDiap,d);
				}
				Console.WriteLine("100 ThreadPool преобразование данных в цикле: {0} секунд",
				                  sw.Elapsed.TotalSeconds);
				hundThreadPoolResults.Add(sw.Elapsed.TotalSeconds);
				sw.Reset();
				for (int i = 0; i < data.Length; i++)
					data[i] = i;
				sw.Start();
				for (int i = 0; i < data.Length; i++)
					MyTransform(i);
				sw.Stop();
				Console.WriteLine("Последовательное преобразование данных в цикле: {0} секунд",
				                  sw.Elapsed.TotalSeconds);
				StupidResults.Add(sw.Elapsed.TotalSeconds);
				Thread.Sleep(1000);
			}
			
			Console.WriteLine("            Parallel.For   Two Threads    100 Threads   100ThreadPool   Stupid");
			for(int i =0;i<iterations;i++)
			{
				Console.WriteLine(i.ToString()+" iteration: "+parForResults[i].ToString()+"    "+ TwoThreadsResults[i].ToString()+"    "+
				                  HundThreadResults[i].ToString()+"    "+hundThreadPoolResults[i].ToString()+"    "+StupidResults[i].ToString());
			}
			
			Console.WriteLine("Average:   "+parForResults.Average().ToString()+"    "+ TwoThreadsResults.Average().ToString()+"    "+
				                  HundThreadResults.Average().ToString()+"    "+hundThreadPoolResults.Average().ToString()+"    "+StupidResults.Average().ToString());
			
			Console.WriteLine("Основной поток завершен");
			Console.ReadLine();
		}
		
		
	}
}